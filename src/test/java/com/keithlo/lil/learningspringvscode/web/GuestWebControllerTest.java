package com.keithlo.lil.learningspringvscode.web;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.keithlo.lil.learningspringvscode.business.service.ReservationService;
import com.keithlo.lil.learningspringvscode.data.entity.Guest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(GuestWebController.class)
public class GuestWebControllerTest {
    
    @MockBean
    private ReservationService reservationService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getHotelGuests() throws Exception{

        List<Guest> guests = new ArrayList<Guest>();

        Guest guest = new Guest();
        guest.setFirstName("JUnit");
        guest.setLastName("Unit");
        guest.setEmailAddress("junit@test.com");
        guest.setPhoneNumber("123-456-789");
        guest.setGuestId(1);

        guests.add(guest);

        given(reservationService.getHotelGuests()).willReturn(guests);

        this.mockMvc.perform(get("/guests"))
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("junit@test.com")));
    }
}
