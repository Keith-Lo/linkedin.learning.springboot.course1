package com.keithlo.lil.learningspringvscode.data.repository;

import com.keithlo.lil.learningspringvscode.data.entity.Room;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomRepository extends CrudRepository<Room, Long>{
    
}
