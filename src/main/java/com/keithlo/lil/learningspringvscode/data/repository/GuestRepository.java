package com.keithlo.lil.learningspringvscode.data.repository;

import com.keithlo.lil.learningspringvscode.data.entity.Guest;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GuestRepository extends CrudRepository<Guest, Long>{
    
}
