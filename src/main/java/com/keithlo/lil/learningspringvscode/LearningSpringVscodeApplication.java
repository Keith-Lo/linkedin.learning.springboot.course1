package com.keithlo.lil.learningspringvscode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearningSpringVscodeApplication {

	public static void main(String[] args) {
		SpringApplication.run(LearningSpringVscodeApplication.class, args);
	}

}
