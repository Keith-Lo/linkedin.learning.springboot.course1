# 1. Introduction

Spring is a popular and fast-growing application framework and inversion-of-control (IOC) container for the Java platform. 

The framework's core features can be used by any Java application and are ideal for enterprise and web app development. 

Learn how to get started with Spring using Spring Boot to build dynamic, data-driven applications. 

This path requires basic fluency in Java, covered in our Become a Java Programmer learning path.
- Build MVC apps in Spring.
- Work with data and REST APIs.
- Secure, test, and deploy your code.

## 1.1. Useful links

- Offical Course Path: [here](https://www.linkedin.com/learning/paths/become-a-spring-developer)
- Study Materials (Video, Sample Codes): [here](https://drive.google.com/drive/folders/1ykVNflLTk3nkj_aiHJ2OB1H-T5xeYfrn)

# 2. Table of Contents

- [1. Introduction](#1-introduction)
  - [1.1. Useful links](#11-useful-links)
- [2. Table of Contents](#2-table-of-contents)
- [3. Chapters](#3-chapters)
  - [3.1. Chapter 1](#31-chapter-1)
    - [3.1.1. Course 2-4](#311-course-2-4)
      - [3.1.1.1. To start the postgres database](#3111-to-start-the-postgres-database)
      - [3.1.1.2. To access postgres database](#3112-to-access-postgres-database)
    - [3.1.2. Course 4-5](#312-course-4-5)
      - [3.1.2.1. Java Library](#3121-java-library)
      - [3.1.2.2. Install JUnit and BDDMockito](#3122-install-junit-and-bddmockito)

# 3. Chapters 

## 3.1. Chapter 1

### 3.1.1. Course 2-4

#### 3.1.1.1. To start the postgres database

Use docker-compose.yml
```
docker-compose up -d
```

#### 3.1.1.2. To access postgres database

Run below command to get into docker container:
```sh
docker exec -it learning-spring-postgres bash
```

Connect into postgres database:
```sh
psql -h localhost -d dev -U postgres -W
```

Select SQL statements;
```sql
\dt+
select * from guest;
```

### 3.1.2. Course 4-5

#### 3.1.2.1. Java Library

#### 3.1.2.2. Install JUnit and BDDMockito

Lecture installed the libs in java. You can do it so or add the lib in pom.xml
